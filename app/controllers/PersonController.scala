package controllers

import javax.inject._

import model.Person
import play.api.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.JsValue
import play.api.mvc._

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class PersonController @Inject() extends Controller {

  val logger = Logger(getClass)

  def actionAddPersonFormEncodedInput: Action[Map[String, Seq[String]]] = Action(parse.urlFormEncoded) { implicit request =>
    val form = PersonController.personForm.bindFromRequest()
    form.value match {
      case maybe : Some[Person] =>
        Person.add(maybe.get)
        Ok(views.html.displaylist(Person.list()))
      case None => BadRequest(s"binding error ${form.errors}")
      case _ => InternalServerError("why here huh ?")
    }

  }

  def actionAddPersonJsonInput: Action[JsValue] = Action(parse.json) { implicit request =>
    logger.debug(s"Body: ${request.body}")
    try {
      val person: Person = request.body.as[Person]
      Person.add(person)
      Ok(views.html.displaylist(Person.list()))
    } catch {
      case e: Throwable =>
        logger.error(s"${e.toString}")
        InternalServerError(s"terrible things have happened. Body: ${request.body}")
    }
  }

  def actionAddForm = Action {
    Ok(views.html.addperson(PersonController.personForm))
  }

  def displayList = Action {
    Ok(views.html.displaylist(Person.list()))
  }

}

object PersonController {
  val personForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "age" -> number(min = 0, max = 120)
    )(Person.apply)(Person.unapply)
  )
}
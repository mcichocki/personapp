package model

import play.api.libs.json.{Json, Reads}

import scala.collection.mutable.ListBuffer

case class Person(name: String, age: Int)
//TODO maybe validation on this level ? How?
object Person {
  private var listBuffer: ListBuffer[Person] = new ListBuffer[Person]()

  def add(p: Person): ListBuffer[Person] = {
    listBuffer += p
  }

  def list(): List[Person] = listBuffer.toList

  implicit val userReads: Reads[Person] = Json.reads[Person]
}
